package com.softtek.courses.integration.service;

import com.softtek.courses.integration.model.User;
import java.util.List;

/**
 *
 * @author Benjamin Cisneros
 */
public interface UserService {

    /**
     *
     * @param id
     * @return
     */
    User findById(long id);

    /**
     *
     * @param name
     * @return
     */
    User findByName(String name);

    /**
     *
     * @param user
     */
    void saveUser(User user);

    /**
     *
     * @param user
     */
    void updateUser(User user);

    /**
     *
     * @param id
     */
    void deleteUserById(long id);

    /**
     *
     * @return
     */
    List<User> findAllUsers();

    /**
     * Delete all the users
     */
    void deleteAllUsers();

    /**
     *
     * @param user
     * @return
     */
    public boolean isUserExist(User user);
}
