package com.softtek.courses.integration.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Benjamin Cisneros
 */
@Controller
@RequestMapping("/")
public class IndexController {

    /**
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.GET)
    public String getIndexPage() {
        return "user_management";
    }

}
