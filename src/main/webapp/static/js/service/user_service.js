'use strict';

angular.module('softtekApp')
        .factory('UserService', ['$http', '$q', function ($http, $q) {
                return {
                    fetchAllUsers: function () {
                        return $http.get('http://localhost:8080/SpringAndAngularIntegrationProject/user/')
                                .then(
                                        function (response) {
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.error('Error while fetching users; ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    createUser: function (user) {
                        return $http.post('http://localhost:8080/SpringAndAngularIntegrationProject/user/', user)
                                .then(
                                        function (response) {
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.error('Error while creating users: ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    updateUser: function (user, id) {
                        return $http.put('http://localhost:8080/SpringAndAngularIntegrationProject/user/' + id, user)
                                .then(
                                        function (response) {
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.error('Error while updating users: ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    },
                    deleteUser: function (id) {
                        return $http.delete('http://localhost:8080/SpringAndAngularIntegrationProject/user/' + id)
                                .then(
                                        function (response) {
                                            return response.data;
                                        },
                                        function (errResponse) {
                                            console.error('Error while deleting user: ' + errResponse);
                                            return $q.reject(errResponse);
                                        }
                                );
                    }
                };
            }]);